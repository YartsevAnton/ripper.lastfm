ripper.lastfm

usage:
lastmf_get_data("your_name", "your_api_key");
	your_name -> lastfm name
	your_api_key -> personal lastfm api key (https://www.last.fm/api/account/create)

<div id="fm_result"></div>
	return data placed to #fm_result
