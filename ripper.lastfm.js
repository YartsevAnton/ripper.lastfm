/*main fnc*/
function lastmf_get_data (user, key) 
{
	var limit = 50; //min-50, max-200
	var page = 1 ;

	/*make request*/
	var get =  "http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user="+user+"&api_key="+key+"&format=json&limit="+limit+"&page="+page+"&extended=1";

	/*request processing*/
	async_get (get, get_handling);
}

/*asynchronic get request*/
function async_get (url, callback)
{
    var xmlHttp = new XMLHttpRequest();

    /*add listeners*/
    xmlHttp.addEventListener("progress", updateProgress, false); 
    //xmlHttp.addEventListener("load", transferComplete, false);
	  //xmlHttp.addEventListener("error", transferFailed, false);
	  //xmlHttp.addEventListener("abort", transferCanceled, false);

    xmlHttp.onreadystatechange = function()
    {
    	/*done, send response to handler*/
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
        {
            callback(xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", url, true); // true for asynchronous 
    xmlHttp.send(null);
}

/*upsate progress*/
function updateProgress (oEvent) 
{
	if (oEvent.lengthComputable)
  	{
   		var percentComplete = oEvent.loaded * 100 / oEvent.total;
		  document.getElementById('progress').value = percentComplete;
  	} 
  	else
  	{
    	document.getElementById('fm_result').innerHTML = "Load data fail.";
  	}
}

function get_handling (response) 
{
	/*out result*/
	response = JSON.parse(response);
	//document.getElementById('fm_result').innerHTML = response.recenttracks.track[23].artist.name;
  document.getElementById('fm_result').append( print(response) );
}


function print (o)
{
    var str='';

    for(var p in o){
        if(typeof o[p] == 'string'){
            str+= p + ': ' + o[p]+';';
        }else{
            str+= p + ': {' + print(o[p]) + '}';
        }
    }

    return str;
}